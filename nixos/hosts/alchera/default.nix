{ config, pkgs, ... }:
{
  imports = [
    ../../configuration.nix
    ./hardware-configuration.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # boot.loader.grub.device = "/dev/sda";

  networking = {
    hostName = "alchera";
    networkmanager = {
      enable   = true;
      packages = [ pkgs.networkmanager ];
    };
  };

}
