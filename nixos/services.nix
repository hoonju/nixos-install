{

  services.openssh.enable = true;
  services.openssh.forwardX11 = true;
  services.openssh.permitRootLogin = "yes";
  services.openssh.passwordAuthentication = true;

  services.geoclue2.enable = true;
  services.tailscale.enable = true;
  services.blueman.enable = true;

  services.xserver = {
    enable = true;

    libinput = {
      enable = true;
      touchpad = {
        disableWhileTyping = true;
        # naturalScrolling = true;
      };
      mouse = {
        disableWhileTyping = true;
        # naturalScrolling = true;
      };
    };

    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
  };
}
