{ pkgs, ... }:
{

  fonts.fontconfig.enable = true;

  console.font = "Lat2-Terminus16";
  console.keyMap = "us";

  fonts.fonts = with pkgs; [
    nerdfonts
    noto-fonts
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    material-design-icons
    weather-icons
    font-awesome
    emacs-all-the-icons-fonts
  ];

}
