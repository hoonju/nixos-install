{ pkgs, ... } : {

  environment = {

    systemPackages = with pkgs; [

      neovim
      coreutils
      curl
      wget
      git

      htop
      pciutils
      agenix

    ];

    # list of acceptable shells in /etc/shells
    shells = with pkgs; [ bashInteractive ];

  };

}
