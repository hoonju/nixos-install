{ pkgs, ... }:
{

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    backupFileExtension = "backup";
  };

  imports = [
    ./nix.nix
    ./fonts.nix
    ./i18n.nix
    ./services.nix
    ./environment.nix
    ./ipfs.nix
    ./virtualbox.nix
    # ./distributed-build.nix
  ];

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  time.timeZone = "Asia/Seoul";

  sound.enable = true;

}
