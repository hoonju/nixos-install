{ pkgs, ... }:

{

  i18n = {
    defaultLocale = "en_US.UTF-8";
    inputMethod.enabled = "ibus";
    inputMethod.ibus.engines = with pkgs.ibus-engines; [ hangul ];
  };

}
