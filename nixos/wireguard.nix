{ wg-key, wg-ips }  : { config, pkgs, ... }:
{

  networking.firewall = {
    allowedUDPPorts = [ 51821 ];
  };

  age.secrets.wg.file = wg-key;

  networking.wireguard.interfaces = {

    hds0 = {

      # Your ip address in the wireguard VPN
      ips = wg-ips;
      listenPort = 51821;

      # Create your wireguard keys
      # ```sh
      # umask 077
      # mkdir ~/.wireguard-keys
      # wg genkey > ~/.wireguard-keys/private
      # wg pubkey < ~/.wireguard-keys/private > ~/.wireguard-keys/public
      # ```
      privateKeyFile = config.age.secrets.wg.path;

      peers = [
        {
          publicKey = "i0ZorMa8S9fT8/TI/U01K5HGhYPGRESnrq36k2I7MBU=";
          allowedIPs = [ "10.10.0.0/16" ];
          endpoint = "121.136.244.64:51821";
          persistentKeepalive = 25;
        }
      ];
    };
  };

}
