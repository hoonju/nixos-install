{ lib, pkgs, config, ... }: {

  nix = {

    package = pkgs.nixFlakes;

    # Enable experimental version of nix with flakes support
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
      ${lib.optionalString (config.nix.package == pkgs.nixFlakes)
      "experimental-features = nix-command flakes"}
    '';

    # CAUTION: Enable binary caches after setting up wireguard
    # If our private cache machines have issue, they can be bypassed by
    # ```sh
    # nix build --option substituters 'https://cache.nixos.org'
    # sudo nixos-rebuild switch --flake .#x1 --option substituters 'https://cache.nixos.org'
    # ````
    binaryCaches = [
      "https://cache.nixos.org/"
    ];

    trustedBinaryCaches = [
      "https://cache.nixos.org/"
    ];

    binaryCachePublicKeys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
    ];

    requireSignedBinaryCaches = false;

    trustedUsers = [ "root" "@admin" "@wheel" "@haedosa" ];

    gc = {
      automatic = false;
      options = "--delete-older-than 30d";
    };
  };

}
