let

  genie = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMFHcAOwifYwNSZXvCKci3ZiWPZIrVzxXY/pJ6+h3aXQ";

  alchera = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOdO1ROBPATkrSLWZONnPvg+PTjlki1VuwWsfcDIrhFe";

  systems = [ alchera ];

in
{

  # pubkey = yfy657gwKqbJXkTtMuTZp9qK0Muccyx7T5mV2DDiIwQ=
  "wg-alchera.age".publicKeys = [ genie alchera ];

  # swarm.key for the private ipfs
  "ipfs-swarm-key.age".publicKeys = [ genie ] ++ systems;

}
