{

  inputs = {
    newpkgs.url = "github:nixos/nixpkgs";
    # nixpkgs.url = "github:nixos/nixpkgs/91ffffd90404331e0af54fa3fb8063f2f849a321";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = { url = "github:nix-community/home-manager/master"; inputs.nixpkgs.follows = "nixpkgs"; };
    flake-utils.url = "github:numtide/flake-utils/master";
    nix-doom-emacs.url = "github:wavetojj/nix-doom-emacs";

    agenix.url = "github:ryantm/agenix";
  };

  outputs = inputs@{
    self,
    newpkgs,
    nixpkgs,
    flake-utils,
    home-manager,
    nix-doom-emacs,
    ...
  }: let

    root = {
      isSystemUser = true;
      hashedPassword = "$6$T80JsrUCydok0S$5/CAsrhK77RRPP3QlqAFjOgjp9CEo/0LUUXwkmT9Tjmsz08DfY5.FkLp3SU3EhlLesH2aq7FGVBBEc07s3R7u/";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINV6j7B2Pt+p4Rud+h1Q+NzdjgA7PJGn+3bMKNtayecS root@x1"
      ];
    };

    hosts = {
      alchera = {
        ip = "genie@192.168.1.104"; # to login; NOTE: before wireguard is up, use a local ip 192.168...
        wg-ips = [ "10.10.1.2/24" ]; # to set up the wireguard,
        wg-key = ./secrets/wg-alchera.age;
        # NOTE: copy the /etc/nixos/hardware-configuration.nix file of x1 under the /nixos/hosts/x1 folder
        configuration = ./nixos/hosts/alchera;
        inherit root;
      };

      legion5 = {
        ip = "10.100.0.22"; # to login; NOTE: before wireguard is up, use a local ip 192.168...
        wg-ips = [ "10.100.0.22/24" ]; # to set up the wireguard
        # NOTE: copy the /etc/nixos/hardware-configuration.nix file of legion5 under the /nixos/hosts/legion5 folder
        configuration = ./nixos/hosts/legion5;
        inherit root;
      };

    };

    users = {

      genie = {
        isNormalUser = true;
        uid = 1000;
        home = "/home/genie";
        extraGroups = [ "wheel" "networkmanager" "audio" "video" "ipfs" ];
        hashedPassword = "$6$W5Y9ymebkq$X5Qh.wSfpfBv3FXct528P1b3MfnOLLq74tonCOIKuWtU/1isYP3pUx1z9Abl8Ov3Lk.KDRM3ZqxaTi2v58mkx/";
        # NOTE: for remote switching to work the corresponding private key has to be in ~/.ssh
        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFsbqCeCuDUP9bOaPG9b7cR7LwfzTgZuPd1qpc654Sli3hbjKve6GJ5DfFH4OM23DOHJ/lBLiqsETWP0LWp91YTR72gWRtrr322C5GHwCWktEiwAXAG5tQUZSf/aZxLxWwSLj2OmzolZFNp8X4T+JO3Juyu9HFHs3y2UQWwV0y0480sgG4+v5kbvTkaJbDeMZgAwCUR2Vv8B1QTObBx37G2wbChueWO03jB6V1p45V6j+XE/qbwiJxL9MzZTC4ZEtTDVOQWbtn78C7CMN7r9kzQOY8XgbyAqMmoZGD+4vyXBKu+Fg5Uf0NZhaVkdazJP6fmoJjy9autonidZ8Gig93 hoonjuchung@yahoo.com"
        ];
      };

    };

    system = "x86_64-linux";
    npkgs = import newpkgs {
      inherit system;
      config = { allowUnfree = true; };
      overlays = [ self.overlay ];
    };

    mkNixOSConfiguration = name: host: nixpkgs.lib.nixosSystem {
      inherit system;
      modules = [

        ({ pkgs, ... }: {
          users.mutableUsers = false;
          users.extraUsers = users // { root = host.root;};
        })
        host.configuration
        (import ./nixos/wireguard.nix { inherit (host) wg-ips wg-key; })
        inputs.agenix.nixosModules.age
        home-manager.nixosModules.home-manager
        {
          nixpkgs = {
            config = {
              allowUnfree = true;
              hostName = name;
            };
            overlays = [
              (self: super: { inherit npkgs; })
              self.overlay
            ];
          };
          home-manager.users =
            (nixpkgs.lib.mapAttrs (username: user:
              {  imports = [ nix-doom-emacs.hmModule
                             ./home
                           ];
              }
            ) users);
        }
      ];
    };

  in {

    overlays = [

      inputs.agenix.overlay

      nix-doom-emacs.overlay
      (import ./overlay.nix { inherit mkNixOSConfiguration hosts; })
    ];

    overlay = nixpkgs.lib.composeManyExtensions self.overlays;

    nixosConfigurations = (nixpkgs.lib.mapAttrs mkNixOSConfiguration hosts);

    nixosPackages = (nixpkgs.lib.mapAttrs
      (name: host: host.config.system.build.toplevel)
      self.nixosConfigurations);


  } // flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        config = {};
        overlays = [ self.overlay ];
      };

    in rec {
      devShell = import ./develop.nix { inherit pkgs; };

      defaultPackage = pkgs.deploy;
      packages = {
        deploy = pkgs.deploy;
      } // self.nixosPackages;

      # nix run .#legion5 to switch legion5 from x1
      # nix run .#legion5 to switch x1 from legion5
      defaultApp = apps.deploy;
      apps = {
        # add other apps here
      } // pkgs.nixOSApps;
    }

  );

}
