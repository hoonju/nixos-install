{ config, pkgs , lib , ... }: {

  xdg.enable = true;

  imports = [
    ./dconf.nix
    ./programs
    ./services
  ];

  home.packages = with pkgs; [

    file
    fd
    unzip
    ncdu
    du-dust
    exa
    ripgrep
    any-nix-shell
    prettyping
    diff-so-fancy
    gnused
    coreutils
    neofetch
    youtube-dl
    syncthing
    rnix-lsp
    imagemagick
    inkscape
    pass
    cmatrix

    xcape                  # keymaps modifier
    xorg.xkbcomp           # keymaps modifier
    google-chrome
    libreoffice

    doom-emacs
    myhaskell
    slack
    alacritty
    zathura

    sublime4
    ffmpeg
    vlc

    cabal-install
    stack

  ];

  home.sessionVariables = {
    EDITOR = "emacsclient -c";
  };

}
