{ config, lib, pkgs, ... }: {

  services.emacs = {
    enable = true;
    package = pkgs.doom-emacs;
    client.enable = true;
  };

}
