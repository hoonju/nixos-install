{ lib, ... }:
let
  mkTuple = lib.hm.gvariant.mkTuple;
in
{
  dconf.settings = {
    "org/gnome/desktop/peripherals/mouse" = {
      "natural-scroll" = true;
      "speed" = -0.5;
    };

    "org/gnome/desktop/peripherals/touchpad" = {
      "natural-scroll" = true;
      "tap-to-click" = true;
      "two-finger-scrolling-enabled" = true;
    };

    "org/gnome/desktop/input-sources" = {
      "sources" = [ (mkTuple [ "ibus" "hangul" ]) ];
      "xkb-options" = [
                        # "caps:ctrl_modifier"
                        # "altwin:swap_lalt_lwin"
                      ];
    };

    "org/gnome/desktop/interface" = {
      "gtk-im-module" = "ibus";
    };

    "org/freedesktop/ibus/engine/hangul" = {
      "hangul-keyboard" = "2f";
      "switch-keys" = "Shift+space";
    };

    "org/gnome/settings-daemon/plugins/power" = {
      "sleep-inactive-ac-type" = "nothing";
      "sleep-inactive-battery-type" = "nothing";
    };

  };
}
