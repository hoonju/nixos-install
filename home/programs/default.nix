{ pkgs, ... }:
{
  programs.home-manager.enable = true;

  programs.bash.enable = true;

  programs.firefox.enable = true;

  programs.git = {
    enable = true;
    userName = "Genie";
    userEmail = "hoonjuchung@yahoo.com";
  };

  programs.direnv = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
    enableFishIntegration = true;
    nix-direnv = {
      enable = true;
      enableFlakes = true;
    };
  };

  # programs.doom-emacs = {
  #   enable = true;
  #   doomPrivateDir = ./doom.d;
  # };
}
