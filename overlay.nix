{ mkNixOSConfiguration, hosts } : final: prev: with final;
{

  doom-emacs = mk-doom-emacs {
    doomPrivateDir = ./home/packages/doom-emacs/doom.d;
  };

  mytex = texlive.combine {
    inherit (texlive)
      collection-basic
      collection-bibtexextra
      collection-latex
      collection-latexextra
      collection-latexrecommended
      collection-binextra
      collection-langenglish
      collection-langkorean
      collection-langcjk
      collection-plaingeneric
      collection-fontutils
      collection-fontsextra
      collection-fontsrecommended
      collection-context
      collection-metapost
      collection-texworks
      collection-luatex
      collection-xetex
      collection-pictures
      collection-pstricks
      collection-publishers
      collection-mathscience
    ;

  };

  myhaskell = haskellPackages.ghcWithPackages (p: with p;
    [ base
      haskell-language-server
      conduit
      conduit-extra
      threadscope
      ghcid
    ]);


  deploy = let
    profile = "/nix/var/nix/profiles/system";
  in writeScriptBin "deploy" ''
    host="$1"
    store="$2"
    # nix-copy-closure --to --use-substitutes $host $store
    nix-copy-closure --to $host $store
    ssh $host sudo nix-env --profile ${profile} --set $store
    ssh $host sudo ${profile}/bin/switch-to-configuration switch
  '';

  nixOSApps = (pkgs.lib.mapAttrs
    (name: host: let
      nixOSConf = mkNixOSConfiguration name host;
      nixOSPkg = nixOSConf.config.system.build.toplevel;
    in
    {
      type = "app";
      program = let
        profile = "/nix/var/nix/profiles/system";
        prog = pkgs.writeScriptBin "deploy" ''
          host="${host.ip}"
          store="${nixOSPkg}"
          # nix-copy-closure --to --use-substitutes $host $store
          nix-copy-closure --to $host $store
          ssh $host sudo nix-env --profile ${profile} --set $store
          ssh $host sudo ${profile}/bin/switch-to-configuration switch
        '';
      in "${prog}/bin/deploy";
    })
    hosts);

}
